package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class User extends Activity {
    public static String apps_list_user="first";
    private SharedPreferences mshared;
    public static final String Pref="My1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
   //     Toast.makeText(this, apps_list_user1+ " welcome", Toast.LENGTH_LONG).show();
        Toast.makeText(this, " welcome Guest", Toast.LENGTH_SHORT).show();

        mshared = getSharedPreferences(Pref,MODE_PRIVATE);
  //      my_list();

    }



    public void showApps(View v){
        Intent i = new Intent(this, AppsListActivity.class);
        startActivity(i);
    }

    public void showApps2(View v){
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, 0);

    }

    public void showApps4(View v){
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.google.com")));
    }


    public void showApps3(View v) {
        Intent galleryIntent = new Intent(Intent.ACTION_VIEW, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivity(galleryIntent);
    }
}
