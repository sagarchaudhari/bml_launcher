package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class AppsListActivity extends Activity{
	public PackageManager manager;
	public List<AppDetail> apps;
    public static String apps_list_user="first";
    private SharedPreferences mshared;
    public static final String Pref="My_A";

    public GridView list;
    ArrayList<String> UserItems = new ArrayList<String>();

    //Button hide, clear;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps_list);
		list = (GridView) findViewById(R.id.apps_list);

        mshared = getSharedPreferences(Pref,MODE_PRIVATE);
        String savedItems = mshared.getString(apps_list_user, "");
        UserItems.addAll(Arrays.asList(savedItems.split(",")));
        new PostTask().execute();

       addClickListener();
    //     my_list();


    }


    private void my_list() {
             String list=mshared.getString(apps_list_user,"");
        //    Toast.makeText(this, list + " welcome", Toast.LENGTH_SHORT).show();
       //      Toast.makeText(AppsListActivity.this, savedItems, Toast.LENGTH_LONG)
    //            .show();
        Toast.makeText(AppsListActivity.this, UserItems.get(1), Toast.LENGTH_LONG)
                .show();


    }
    private void addClickListener(){
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos,
                                    long id) {
                Intent i = manager.getLaunchIntentForPackage(apps.get(pos).name.toString());
                AppsListActivity.this.startActivity(i);
            }
        });
    }




    // The definition of our task class
	class PostTask extends AsyncTask<String, Integer, String> {

		ArrayAdapter<AppDetail> adapter;


        AppDetail k;

		String k1;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displayProgressBar("loading...Apps");
			// Toast.makeText(this, "welcome man", Toast.LENGTH_LONG).show();
	}

		private void displayProgressBar(String string) {
			// TODO Auto-generated method stub

		}

		@Override
		protected String doInBackground(String... params) {

            String s;
			manager = getPackageManager();
			apps = new ArrayList<AppDetail>();
			// ComponentName componentName; // activity which is first time open
			// in manifiest file which is declare as <category
			// android:name="android.intent.category.LAUNCHER" />
			// componentName = new ComponentName(this, MainActivity.class);
			// manager.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
			// PackageManager.DONT_KILL_APP);

			Intent i = new Intent(Intent.ACTION_MAIN, null);

			i.addCategory(Intent.CATEGORY_LAUNCHER);

			List<ResolveInfo> availableActivities = manager
					.queryIntentActivities(i, 0);
			for (ResolveInfo ri : availableActivities) {
				AppDetail app = new AppDetail();
                s=(String)ri.loadLabel(manager);
              //  Toast.makeText(AppsListActivity.this, s, Toast.LENGTH_LONG)
                //        .show();
                if(UserItems.contains(s)){
                    continue;
                }


                app.label = (String) ri.loadLabel(manager);
				app.name = ri.activityInfo.packageName;
				app.icon = ri.activityInfo.loadIcon(manager);
				// app.check = ri.toString();
				
			//	app.check = false;
				apps.add(app);

			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
             String s;

			adapter = new ArrayAdapter<AppDetail>(AppsListActivity.this,R.layout.list_item, apps) {

				
				
				
	/*
	class myViewHolder {
		ImageView app_image;
		TextView app_lab;
		TextView app_name;
		CheckBox ch;

		myViewHolder(View v) {
			app_image = (ImageView) v.findViewById(R.id.item_app_icon);
			app_lab = (TextView) v.findViewById(R.id.item_app_label);
			app_name = (TextView) v.findViewById(R.id.item_app_name);
			ch = (CheckBox) v.findViewById(R.id.cb);

		}

	}
	
	/*
	 	myViewHolder holder = null;
					if (row == null) {
						convertView = getLayoutInflater().inflate(
								R.layout.list_item, parent, false);
						holder = new myViewHolder(row);
						row.setTag(holder);
					} else {
						holder = (myViewHolder) row.getTag();

					}

					k = apps.get(position);

					String ss;
					ss = k.name;

					holder.app_image.setImageDrawable(k.icon);

					holder.app_lab.setText(k.label);
					holder.app_name.setText(k.name);

					holder.ch.setChecked(apps.get(position).check);

					
	 */

				
				
				
				@Override
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View row = convertView;
					if (row == null) {
						convertView = getLayoutInflater().inflate(
								R.layout.list_item,parent,false);
					}
					

					
					k=apps.get(position);
					ImageView appIcon = (ImageView) convertView
							.findViewById(R.id.item_app_icon);
					appIcon.setImageDrawable(k.icon);

					TextView appLabel = (TextView) convertView
							.findViewById(R.id.item_app_label);
					appLabel.setText(k.label);

				//	TextView appName = (TextView) convertView
				//			.findViewById(R.id.item_app_name);
				//	appName.setText(k.name);


					 String ss;
					 ss = k.name;
			//		  ch = (CheckBox) convertView.findViewById(R.id.cb);
				//	 ch.setChecked(apps.get(position).check);
						


						


					return convertView;
				}

			};
			list.setAdapter(adapter);

		}

	}

}
