package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by SIMPRO on 12/3/2014.
 */
public class Welcome extends Activity {

    EditText et_password;
    private SharedPreferences mshared;
    public static final String Pref="admin_data";
    private static int k=0;
    final Context context = this;


     public static final String new_key="new_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        et_password = (EditText)findViewById(R.id.et_password);
        mshared = getSharedPreferences(Pref,MODE_PRIVATE);
        Button b = (Button)findViewById(R.id.b_welcome);
        b.setBackgroundResource(R.drawable.shapes2);
  //       show_password();

    }

    private void show_password() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle("Android Custom Dialog Box");

       final EditText et_new_one = (EditText)findViewById(R.id.et_new_one);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButton);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String one = et_new_one.getText().toString();
                SharedPreferences.Editor editor = mshared.edit();
                if(one.isEmpty()){
                    Toast.makeText(Welcome.this, "Please enter new password", Toast.LENGTH_SHORT).show();

                }
                else {
                    k = 1;
                    editor.putString(new_key, one);
                    if (editor.commit()) {
                        Toast.makeText(Welcome.this, "new password is " + one, Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                    et_new_one.setText("");
                }
            }
        });

        dialog.show();


    }


    public void User_identify(View v){

        String password1 = mshared.getString(new_key,"");
        String password=et_password.getText().toString();
        et_password.setText("");

        if(password.isEmpty()){
            Toast.makeText(Welcome.this, "Please, Enter the password..", Toast.LENGTH_SHORT).show();

        }
        else if(password.equals(password1)){
            Intent i = new Intent(this, Admin.class);
            startActivity(i);
        }
        else{
            Intent i = new Intent(this, User.class);
            startActivity(i);
        }

    }





}
