package bmlmedia.com.bmlmain;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SIMPRO on 12/14/2014.
 */

public class MainAct extends ActionBarActivity {
    ListView list;
    ListViewAdapter listviewadapter;
    List<WorldPopulation> worldpopulationlist = new ArrayList<WorldPopulation>();
    String app_name="";
    String   savedItems="";
    WorldPopulation world;
    private PackageManager manager;

    SharedPreferences sharedpreferences;
    public static final String My1 ="My_A";
   public static final String apps_list_user11="first";
   SharedPreferences.Editor pre1;

    ArrayList<String> selectedItems = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_act_hide);
        toasto();


        sharedpreferences = getSharedPreferences(My1, Context.MODE_PRIVATE);

        manager = getPackageManager();
        //   apps = new ArrayList<AppDetail>();
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> availableActivities = manager
                .queryIntentActivities(i, 0);
        for (ResolveInfo ri : availableActivities) {
            world = new WorldPopulation();

            world.setRank((String) ri.loadLabel(manager));
            world.setCountry(ri.activityInfo.packageName);
            world.setFlag(ri.activityInfo.loadIcon(manager));
            worldpopulationlist.add(world);
        }



        list = (ListView) findViewById(R.id.listview);
        list.setDividerHeight(10);
        // Pass results to ListViewAdapter Class
        listviewadapter = new ListViewAdapter(this, R.layout.hide_items,
                worldpopulationlist);

        // Binds the Adapter to the ListView
        list.setAdapter(listviewadapter);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        // Capture ListView item click
        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode,
                                                  int position, long id, boolean checked) {
                // Capture total checked items
                final int checkedCount = list.getCheckedItemCount();
                // Set the CAB title according to total checked items
                mode.setTitle(checkedCount + " Selected");
                //mode.getMenu().removeItem(0);
                // Calls toggleSelection method from ListViewAdapter Class
                listviewadapter.toggleSelection(position);
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete:
                        // Calls getSelectedIds method from ListViewAdapter Class
                        SparseBooleanArray selected = listviewadapter
                                .getSelectedIds();
                        sharedpreferences.edit().clear().commit();
                        // Captures all selected ids with a loop
                        for (int i = (selected.size() - 1); i >= 0; i--) {
                            if (selected.valueAt(i)) {
                                WorldPopulation selecteditem = listviewadapter
                                        .getItem(selected.keyAt(i));
                                // Remove selected items following the ids
                                list.getItemAtPosition(i);
                                app_name += selecteditem.getRank().toString()+",";
                                //listviewadapter.remove(selecteditem);

                            }
                        }
                        // Close CAB
                        //   mode.finish();
                        pre1 = sharedpreferences.edit();
                        pre1.putString(apps_list_user11, app_name);
                        pre1.commit();
                        Toast.makeText(MainAct.this, app_name + " ", Toast.LENGTH_SHORT).show();

                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.hide_menu, menu);


                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // TODO Auto-generated method stub
                //     listviewadapter.removeSelection();
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }
        });
       load();
    }

    private void toasto() {
        Toast toast= Toast.makeText(getApplicationContext(),
                "LongClick to Select the apps ", Toast.LENGTH_LONG);

        toast.setGravity(Gravity.TOP , 0,20);
        toast.show();

    }

    public void Clear_all(View v){
        sharedpreferences.edit().clear().commit();
        int count = list.getAdapter().getCount();
        for (int i = 0; i < count; i++) {
            list.setItemChecked(i, false);
        }

        Toast.makeText(MainAct.this, "list cleared", Toast.LENGTH_LONG)
                .show();

    }

    private void load() {
         savedItems="";
         String s;
        savedItems = sharedpreferences.getString(apps_list_user11, "");
        selectedItems.addAll(Arrays.asList(savedItems.split(",")));
        //      Toast.makeText(MainActivity.this, savedItems, Toast.LENGTH_LONG)
        //               .show();

        int count = this.list.getAdapter().getCount();
        //       Toast.makeText(MainActivity.this, count+"", Toast.LENGTH_LONG)
        //             .show();

        for (int i = 0; i < count; i++) {
            WorldPopulation currentItem = (WorldPopulation) list.getAdapter().getItem(i);
            s=currentItem.getRank();
            if (selectedItems.contains(s)) {
                    list.setItemChecked(i, true);


            } else {
                   // list.setItemChecked(i, false);
            }

        }




    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
