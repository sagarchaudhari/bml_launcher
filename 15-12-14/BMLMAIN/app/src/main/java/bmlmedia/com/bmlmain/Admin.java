package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class Admin extends Activity {

    Button b_home,b_setting,b_logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin);
     //   MY_APPS();
        b_home = (Button)findViewById(R.id.b_admin1);
        b_setting = (Button)findViewById(R.id.b_admin2);
        b_logout = (Button)findViewById(R.id.b_admin3);

         b_home.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 showApps111();
             }
         });
        b_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showApps211();
            }
        });
        b_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showApps311();
            }
        });
    }

    public void showApps111(){
        Intent i = new Intent(this, AppsListActivity1.class);
        startActivity(i);
    }

    public void showApps211(){
        Intent i2 = new Intent(this, Settings.class);
        startActivity(i2);
    }
    public void showApps311(){
        Intent i3 = new Intent(this, Welcome.class);
        startActivity(i3);

    }


}
