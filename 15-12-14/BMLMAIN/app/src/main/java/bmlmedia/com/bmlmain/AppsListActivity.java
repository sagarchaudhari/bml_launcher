package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class AppsListActivity extends Activity{

    public PackageManager manager;
	public List<AppDetail> apps;

    public static String apps_list_user="first";
    private SharedPreferences mshared;
    public static final String Pref="My_A";

    public GridView list;
    ArrayList<String> UserItems = new ArrayList<String>();

    //Button hide, clear;

    private BroadcastReceiver mReceiver;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps_list);
		list = (GridView) findViewById(R.id.apps_list);

        mshared = getSharedPreferences(Pref,MODE_PRIVATE);
        String savedItems = mshared.getString(apps_list_user, "");


        UserItems.addAll(Arrays.asList(savedItems.split(",")));
        Collections.sort(UserItems);
        new PostTask().execute();


       addClickListener();
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);

        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);

// Customized BroadcastReceiver class
//Will be defined soon..

        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);


    }


    private void addClickListener(){
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos,
                                    long id) {
                String lab = apps.get(pos).name.toString();
                Intent i = manager.getLaunchIntentForPackage(apps.get(pos).name.toString());
                AppsListActivity.this.startActivity(i);
            }
        });
    }




    // The definition of our task class
	class PostTask extends AsyncTask<String, Integer, String> {

		ArrayAdapter<AppDetail> adapter;


        AppDetail k;

		String k1;

		@Override
		protected String doInBackground(String... params) {

            String s;
			manager = getPackageManager();
			apps = new ArrayList<AppDetail>();

			Intent i = new Intent(Intent.ACTION_MAIN, null);
            i.addCategory(Intent.CATEGORY_LAUNCHER);

            Collections.sort(UserItems);
			List<ResolveInfo> availableActivities = manager
					.queryIntentActivities(i, 0);
            for (ResolveInfo ri : availableActivities) {
				AppDetail app = new AppDetail();
                s=(String)ri.loadLabel(manager);

                if(UserItems.contains(s)){
                    continue;
                }


                app.label = (String) ri.loadLabel(manager);
				app.name = ri.activityInfo.packageName;
				app.icon = ri.activityInfo.loadIcon(manager);
				// app.check = ri.toString();
				
			//	app.check = false;
				apps.add(app);

			}


            return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
             String s;
           adapter = new ArrayAdapter<AppDetail>(AppsListActivity.this,R.layout.list_item, apps) {



				@Override
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View row = convertView;

                    myViewHolder holder = null;
                    if (row == null) {
                        row = getLayoutInflater().inflate(  R.layout.list_item, parent, false);
                        holder = new myViewHolder(row);
                        row.setTag(holder);
                    } else {
                        holder = (myViewHolder) row.getTag();

                    }

                   k = apps.get(position);
                    holder.app_image.setImageDrawable(k.icon);
                    holder.app_lab.setText(k.label);


					return row;
				}

			};

			list.setAdapter(adapter);

		}

	}


    class myViewHolder {
        ImageView app_image;
        TextView app_lab;


        myViewHolder(View v) {
            app_image = (ImageView) v.findViewById(R.id.item_app_icon);
            app_lab = (TextView) v.findViewById(R.id.item_app_label);
            //   app_name = (TextView) v.findViewById(R.id.item_app_name);
            //   ch = (CheckBox) v.findViewById(R.id.cb);

        }

    }

    public class ScreenReceiver extends android.content.BroadcastReceiver {

        Intent i3;

        @Override
        public void onReceive(Context context, Intent intent) {
           if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                Log.v("$$$$$$", "In Method:  ACTION_USER_PRESENT");
//Handle resuming events
                //     Toast.makeText(Welcome.this, "you are ACTION_USER_PRESENT", Toast.LENGTH_SHORT).show();
                again();


            }

        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.v("$$$$$$", "In Method: onDestroy()");

        if (mReceiver != null)
        {
            unregisterReceiver(mReceiver);
            mReceiver = null;
        }

    }

    private void again() {
        Intent i3 = new Intent(this, Welcome.class);
        startActivity(i3);
     }



}
