package bmlmedia.com.bmlmain;

import android.app.WallpaperManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by SIMPRO on 12/15/2014.
 */
public class Set_Wallpaper  extends ActionBarActivity {
    private static final int SELECT_PICTURE = 1;
    private ImageView img;
    Uri selectedImageUri;
    String picturePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_wall);
        img = (ImageView)findViewById(R.id.ImageView01);

        ((Button) findViewById(R.id.Button01))
                .setOnClickListener(new View.OnClickListener() {

                    public void onClick(View arg0) {

                        // in onCreate or any event where your want the user to
                        // select a file
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,
                                "Select Picture"), SELECT_PICTURE);

                    }
                });

        ((Button) findViewById(R.id.b_set))
                .setOnClickListener(new View.OnClickListener() {

                    public void onClick(View arg0) {
                        try{
                            FileInputStream is = new FileInputStream(new File(picturePath));
                            BufferedInputStream bis = new BufferedInputStream(is);
                            Bitmap b = BitmapFactory.decodeStream(bis);

                            if(!("").equals(picturePath)){
                                WallpaperManager wallpaperManager = WallpaperManager.getInstance(Set_Wallpaper.this);
                                wallpaperManager.setBitmap(b);
                            }
                            Toast.makeText(Set_Wallpaper.this, is + "WallPaper set",
                                    Toast.LENGTH_SHORT).show();
                        }catch(Exception e1){
                            Toast.makeText(Set_Wallpaper.this, e1 + " ",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {


                selectedImageUri = data.getData();
                String [] filepath ={MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImageUri,filepath, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filepath[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();

                img.setImageURI(selectedImageUri);
                Toast.makeText(Set_Wallpaper.this, picturePath + " path",
                        Toast.LENGTH_SHORT).show();


            }
        }
    }


}
