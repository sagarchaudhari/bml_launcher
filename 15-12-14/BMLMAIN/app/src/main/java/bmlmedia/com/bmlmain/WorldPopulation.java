package bmlmedia.com.bmlmain;

import android.graphics.drawable.Drawable;

/**
 * Created by SIMPRO on 12/12/2014.
 */
public class WorldPopulation {
    private String name;
    private String label;
    private Drawable flag;

    public WorldPopulation() {

    }



    public String getRank() {
        return name;
    }

    public void setRank(String rank) {
        this.name = rank;
    }

    public String getCountry() {
        return label;
    }

    public void setCountry(String country) {
        this.label = country;
    }


    public Drawable getFlag() {
        return flag;
    }

    public void setFlag(Drawable flag) {
        this.flag = flag;
    }

}
