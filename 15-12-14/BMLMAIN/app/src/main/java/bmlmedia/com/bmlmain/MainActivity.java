package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener {


    public ListView mylist;
    private PackageManager manager;
    public static String s;
    SharedPreferences sharedpreferences;
    public static final String My1 ="My_A";
    String savedItems;
    //   String selected = "";
    public static final String apps_list_user11="first";
    ArrayAdapter<String> adapter;

    ArrayList<String> app_string = new ArrayList<String>();
    ArrayList<String> selectedItems = new ArrayList<String>();
    ArrayList<String> UserItems = new ArrayList<String>();
    SharedPreferences.Editor pre1;
    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mylist = (ListView) findViewById(R.id.list_xml);
        mylist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        Button hide = (Button) findViewById(R.id.getchoice);
        hide.setOnClickListener(this);
        Button select = (Button) findViewById(R.id.clear_all);
        select.setOnClickListener(this);
        hide.setBackgroundResource(R.drawable.shapes);
        sharedpreferences = getSharedPreferences(My1, Context.MODE_PRIVATE);
        new PostTask().execute();

    }


    private   class PostTask extends AsyncTask<String, Integer, String> {
        ArrayAdapter<String> adapter;

        @Override
        protected String doInBackground(String... strings) {
            Load_data();



            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Load_View();
            LoadSelections();

        }
    }


    private void Load_View() {
        adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_multiple_choice, app_string) { };

        mylist.setAdapter(adapter);
        savedItems = sharedpreferences.getString(apps_list_user11, "");

    }

    private void Load_data() {

        manager = getPackageManager();
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> availableActivities = manager.queryIntentActivities(
                i, 0);

        for (ResolveInfo ri : availableActivities) {
            AppDetail app = new AppDetail();
            String ri1 = ri.activityInfo.packageName;
            String r0 = (String) ri.loadLabel(manager);

            app_string.add(r0);

        }

    }


    private void LoadSelections() {
        // if the selections were previously saved load them

        //      Toast.makeText(MainActivity.this, savedItems, Toast.LENGTH_LONG)
        //            .show();

        selectedItems.addAll(Arrays.asList(savedItems.split(",")));
        // Toast.makeText(MainActivity.this, selectedItems.get(1), Toast.LENGTH_LONG)
        //            .show();
        Collections.sort(selectedItems);

        int count = this.mylist.getAdapter().getCount();

        for (int i = 0; i < count; i++) {
            String currentItem = (String) mylist.getAdapter().getItem(i);
            if (selectedItems.contains(currentItem)) {
                mylist.setItemChecked(i, true);
            } else {
                mylist.setItemChecked(i, false);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getchoice:
                int cntChoice = mylist.getCount();
                String s="";
                sharedpreferences.edit().clear().commit();

                SparseBooleanArray sparseBooleanArray = mylist
                        .getCheckedItemPositions();
                for (int i = 0; i < cntChoice; i++) {
                    if (sparseBooleanArray.get(i)) {
                        s += mylist.getItemAtPosition(i).toString() + ",";
                        //  System.out.println("Checking list while adding:"
                        //        + mylist.getItemAtPosition(i).toString());
                        //          SaveSelections();

                    }

                }
                // s=selected;

                pre1 = sharedpreferences.edit();
                pre1.putString(apps_list_user11, s);
                pre1.commit();



                ////
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG)
                        .show();

                break;
            case R.id.clear_all:
                int count = this.mylist.getAdapter().getCount();
                for (int i = 0; i < count; i++) {
                    this.mylist.setItemChecked(i, false);
                }
                sharedpreferences.edit().clear().commit();

                break;


        }

    }


}



