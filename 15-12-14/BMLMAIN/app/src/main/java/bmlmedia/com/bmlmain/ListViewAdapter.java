package bmlmedia.com.bmlmain;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SIMPRO on 12/12/2014.
 */
public class ListViewAdapter extends ArrayAdapter<WorldPopulation> {
    Context context;
    LayoutInflater inflater;
    List<WorldPopulation> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resourceId,
                           List<WorldPopulation> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView name;
        TextView label;
        ImageView flag;
    }
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        //View row = viewiew;
        WorldPopulation w = worldpopulationlist.get(position);

        if (view == null) {
            view = inflater.inflate(R.layout.hide_items, null);

        }
        ImageView appIcon = (ImageView) view
                .findViewById(R.id.flag);
        appIcon.setImageDrawable(w.getFlag());

        TextView appLabel = (TextView) view
                .findViewById(R.id.rank);
        appLabel.setText(w.getRank());

        TextView appName = (TextView) view
                .findViewById(R.id.country);
        appName.setText(w.getCountry());

        return view;
    }
    @Override
    public void remove(WorldPopulation object) {
        worldpopulationlist.remove(object);
        notifyDataSetChanged();
    }


    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
