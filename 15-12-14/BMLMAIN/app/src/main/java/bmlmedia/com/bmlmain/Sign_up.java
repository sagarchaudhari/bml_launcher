package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class Sign_up extends Activity implements View.OnClickListener {

    EditText et_old,et_new;
    Button b_save;

    private SharedPreferences mshared;
    public static final String Pref="admin_data";
    public static final String old_key="old_key";
    public static final String new_key="new_key";



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password);
        b_save = (Button) findViewById(R.id.b_admin_pass);
        et_new = (EditText)findViewById(R.id.et_password_new);
        et_old = (EditText) findViewById(R.id.et_password_old);
        b_save.setOnClickListener(Sign_up.this);

        mshared = getSharedPreferences(Pref,MODE_PRIVATE);


    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.b_admin_pass:
                String f_name = et_old.getText().toString();
                String s_name = et_new.getText().toString();
                 String old_pass = mshared.getString(new_key,"");


                if(f_name.equals("") && s_name.equals("")){
                    Toast.makeText(Sign_up.this, "Kindly , Enter the password", Toast.LENGTH_SHORT).show();


                }else if(s_name.isEmpty()){
                    Toast.makeText(Sign_up.this, "Kindly , Enter the NEW password", Toast.LENGTH_SHORT).show();



                }else if(s_name.equals(f_name)){
                    Toast.makeText(Sign_up.this, "Same Password Saved", Toast.LENGTH_SHORT).show();
                    et_old.setText("");
                    et_new.setText("");

                }
                else if(f_name.isEmpty()){
                    Toast.makeText(Sign_up.this, "Kindly , Enter the Old password", Toast.LENGTH_SHORT).show();

                }
                else{

                    if(old_pass.equals(f_name)) {
                        SharedPreferences.Editor editor = mshared.edit();
                        editor.putString(old_key, f_name);
                        editor.putString(new_key, s_name);

                        if (editor.commit()) {
                            Toast.makeText(Sign_up.this, "new password Saved", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else
                    {
                        Toast.makeText(Sign_up.this, "Old Password Wrong..", Toast.LENGTH_SHORT).show();
                    }
                    et_old.setText("");
                    et_new.setText("");

                }
              /*  else {


                }
                */break;
        }
    }
}
