package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * Created by SIMPRO on 12/16/2014.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

/**
 * Created by SIMPRO on 12/15/2014.
 */
public class Set_Wall_Admin extends Activity{
    private static final int TAKE_PHOTO_CODE = 1;

    ImageView iv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.set_admin_wall);
     takePhoto();
         iv = (ImageView)findViewById(R.id.iv);

    }

    private void takePhoto(){
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getFile(this)) );
        startActivityForResult(intent, TAKE_PHOTO_CODE);
    }
    private File getFile(Context context){

        final File path = new File( Environment.getExternalStorageDirectory(), "Image keeper" );
        if(!path.exists()){
            path.mkdir();
        }
        String name;
        int n = 100000;
        int rand;
        rand = new Random().nextInt(n);
        name = "Image-" + rand + ".jpg";
        File file = new File(path,name);

        return file;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch(requestCode){
                case TAKE_PHOTO_CODE:
                    final File file = getFile(this);
                    try {
                        Bitmap captureBmp;
                        captureBmp = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(file));

                       iv.setImageBitmap(captureBmp);
                        iv.setImageURI(Uri.fromFile(file));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
