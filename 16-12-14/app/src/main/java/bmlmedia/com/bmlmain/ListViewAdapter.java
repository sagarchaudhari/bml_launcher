package bmlmedia.com.bmlmain;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by SIMPRO on 12/12/2014.
 */
public class ListViewAdapter extends ArrayAdapter<Hide_Data> {
    Context context;
    LayoutInflater inflater;
    List<Hide_Data> worldpopulationlist;
    private SparseBooleanArray mSelectedItemsIds;

    public ListViewAdapter(Context context, int resourceId,
                           List<Hide_Data> worldpopulationlist) {
        super(context, resourceId, worldpopulationlist);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.worldpopulationlist = worldpopulationlist;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView name;
        TextView label;
        ImageView flag;

        ViewHolder(View v){
            name  = (TextView) v
                    .findViewById(R.id.country);

            label =(TextView) v
                    .findViewById(R.id.rank);

            flag = (ImageView) v
                    .findViewById(R.id.flag);
        }
    }
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder=null;
        //View row = viewiew;
        Hide_Data w = worldpopulationlist.get(position);

        if (view == null) {

            view = inflater.inflate(R.layout.hide_items, null);
            holder = new ViewHolder(view);
            view.setTag(holder);

        }
        else{
            holder = (ViewHolder)view.getTag();
        }

        holder.flag.setImageDrawable(w.getFlag());
        holder.flag.setBackgroundResource(R.drawable.shapes4);
   //     ImageView appIcon = (ImageView) view
      //          .findViewById(R.id.flag);
    //    appIcon.setImageDrawable(w.getFlag());

        holder.label.setText(w.getRank());
        holder.name.setText(w.getCountry());
/*
        TextView appLabel = (TextView) view
                .findViewById(R.id.rank);
        appLabel.setText(w.getRank());

        TextView appName = (TextView) view
                .findViewById(R.id.country);
        appName.setText(w.getCountry());
*/
        return view;
    }





    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }


    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}
