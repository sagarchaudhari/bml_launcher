package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by SIMPRO on 12/3/2014.
 */
public class Welcome extends Activity {

    EditText et_password;

    private SharedPreferences mshared;
    public static final String Pref="admin_data";
    public static final String new_key="new_key";
    public static final String pass_key="pass_key";

    final Context context = this;
    private BroadcastReceiver mReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.welcome);

        et_password = (EditText)findViewById(R.id.et_password);
        mshared = getSharedPreferences(Pref,MODE_PRIVATE);

        Button b = (Button)findViewById(R.id.b_welcome);

        show_password();





        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);

        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_USER_PRESENT);


        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);




    }

class ScreenReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
            Log.v("$$$$$$", "In Method:  ACTION_USER_PRESENT");
             again();

        }

    }
}

    private void again() {
        Intent i3 = new Intent(this, Welcome.class);
        startActivity(i3);

    }


    private void show_password() {

        String pass = mshared.getString(pass_key,"");

        if(pass.equals("log_in"))
        {

      }
        else{
            String d="log_in";
            SharedPreferences.Editor editor1 = mshared.edit();
            editor1.putString(pass_key,d);
            editor1.commit();
            Intent fi = new Intent(this, First_time.class);

            startActivity(fi);
            this.finish();

        }

    }


    public void User_identify(View v){

        String password1 = mshared.getString(new_key,"");
        String password=et_password.getText().toString();
        et_password.setText("");

      if(password.equals(password1)){
            Intent i = new Intent(this, Admin.class);
            startActivity(i);

        }
        else{
            Intent i = new Intent(this, User.class);
            startActivity(i);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

         Log.v("$$$$$$", "In Method: onDestroy()");

            if (mReceiver != null)
            {
                unregisterReceiver(mReceiver);
                mReceiver = null;
            }

        }
    }

