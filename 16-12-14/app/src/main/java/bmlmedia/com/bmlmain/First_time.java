package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by SIMPRO on 12/9/2014.
 */
public class First_time extends Activity {
    EditText et_new_one;
    private SharedPreferences mshared;
    public static final String Pref="admin_data";


    private static int k=0;
    final Context context = this;

    public static final String new_key="new_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog);
        et_new_one = (EditText)findViewById(R.id.et_new_one);
        mshared = getSharedPreferences(Pref,MODE_PRIVATE);

        Button dialogButton = (Button)findViewById(R.id.dialogButton);
        dialogButton.setBackgroundResource(R.drawable.shapes2);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String one = et_new_one.getText().toString();
                SharedPreferences.Editor editor = mshared.edit();
                if (one.isEmpty()) {
                    Toast.makeText(First_time.this, "Please enter new password", Toast.LENGTH_SHORT).show();

                } else {
                    k = 1;
                    editor.putString(new_key, one);
                    if (editor.commit()) {
                        Toast.makeText(First_time.this, "new password is " + one, Toast.LENGTH_SHORT).show();
                    }
                    et_new_one.setText("");

                    Intent i = new Intent(First_time.this, Admin.class);
                    startActivity(i);

                    finish();
                }

            }
        });
    }

}