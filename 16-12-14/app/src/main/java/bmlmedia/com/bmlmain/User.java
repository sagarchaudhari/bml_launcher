package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class User extends Activity{

    Button b_app,b_bro;
//    Button b_cam,b_gal;
    RelativeLayout l1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        l1 = (RelativeLayout)findViewById(R.id.w2);
        Toast toast= Toast.makeText(getApplicationContext(),
                "Welcome Guest", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER| Gravity.CENTER_HORIZONTAL, 0, 0);

        toast.show();

        b_app = (Button)findViewById(R.id.b_app);
     //   b_cam  = (Button)findViewById(R.id.b_cam);
     //   b_gal = (Button)findViewById(R.id.b_gal);
        b_bro = (Button)findViewById(R.id.b_bro);

     //   b_app.setBackgroundResource(R.drawable.shapes3);
    l1.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            Intent i = new Intent(User.this, Show_Settings.class);
            startActivity(i);
            return false;
        }
    });

    }

    public void showSettings(View v){
        Intent i = new Intent(this, Set_Wallpaper.class);
        startActivity(i);
    }


    public void showApps(View v){
        Intent i = new Intent(this, AppsListActivity.class);
        startActivity(i);
    }

    /*

        <Button
        android:layout_width="wrap_content"
            android:layout_weight=".20"
            android:layout_margin="5dp"
            android:background="@drawable/shapes3"
            android:id="@+id/b_cam"
        android:layout_height="wrap_content"
            android:onClick="showApps2"
        android:text="CAMERA"/>

        <Button
        android:layout_width="wrap_content"
            android:background="@drawable/shapes3"
            android:id="@+id/b_gal"
            android:layout_margin="5dp"
            android:layout_weight=".20"
        android:layout_height="wrap_content"
            android:onClick="showApps3"
        android:text="GALLERY"/>

    public void showApps2(View v){
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, 0);

    }

    public void showApps3(View v) {
        Intent galleryIntent = new Intent(Intent.ACTION_VIEW, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivity(galleryIntent);
    }
*/
    public void showApps4(View v){
        Intent i = new Intent(this, Welcome.class);
        startActivity(i);
    }



}
