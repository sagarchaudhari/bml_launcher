package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * Created by SIMPRO on 12/16/2014.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

/**
 * Created by SIMPRO on 12/15/2014.
 */
public class Set_Wall_Admin extends Activity{
    protected Button imgButton;
    protected ImageView image;
    protected TextView field;
    protected Button wallpapaerBtn;

    private boolean imgCapFlag = false;
    protected boolean taken;
    protected static final String PHOTO_TAKEN = "photo_taken";
    protected String path;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_admin_wall);
        image = (ImageView) findViewById(R.id.image);
        field = (TextView) findViewById(R.id.field);
        imgButton = (Button) findViewById(R.id.button);
        wallpapaerBtn = (Button) findViewById(R.id.button1);
        imgButton.setOnClickListener(new ButtonClickHandler());
        wallpapaerBtn.setOnClickListener(new ButtonClickHandler1());
        path = Environment.getExternalStorageDirectory()
                + "/DCIM/Camera/make_machine_example.jpg";

    }

    public class ButtonClickHandler implements View.OnClickListener {
        public void onClick(View view) {
            // Below log message will be logged when you click Take photo button
            Log.i("AndroidProgrammerGuru", "ButtonClickHandler.onClick()");
            // Call startCameraActivity, an user defined method, going to be
            // created
            startCameraActivity();
        }
    }

    public class ButtonClickHandler1 implements View.OnClickListener {
        public void onClick(View view) {
            //Log message
            Log.i("AndroidProgrammerGuru", "ButtonClickHandler.onClick()");
            try {
                //Check if image is captured which sets
                //imgCapFlag in onPhotoTaken method
                if (imgCapFlag) {
                    BitmapDrawable drawable = (BitmapDrawable) image
                            .getDrawable();
                    Bitmap bitmap = drawable.getBitmap();


                    WallpaperManager wallpaperManager = WallpaperManager
                            .getInstance(Set_Wall_Admin.this);

                    Display display = getWindowManager().getDefaultDisplay();

                    final int maxWidth = display.getWidth();
                    final int maxHeight = display.getHeight();



                    int imageHeight = bitmap.getHeight();
                    if ( imageHeight > maxHeight )
                        imageHeight = maxHeight;
                    int imageWidth = (imageHeight*bitmap.getWidth()) / bitmap.getHeight();
                    if ( imageWidth > maxWidth ) {
                        imageWidth = maxWidth;
                        imageHeight = (imageWidth*bitmap.getHeight()) / bitmap.getWidth();
                    }


                    Bitmap resizedBitmap = Bitmap.createScaledBitmap( bitmap, imageWidth, imageHeight, true);

                    wallpaperManager.setBitmap(resizedBitmap);Toast.makeText(Set_Wall_Admin.this,
                            "Wallpaper set : )", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(
                            Set_Wall_Admin.this,
                            "You must capture photo before  you try to set wallpaper!",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void startCameraActivity() {
        // Log message
        Log.i("AndroidProgrammerGuru", "startCameraActivity()");
        // Create new file with name mentioned in the path variable
        File file = new File(path);
        // Creates a Uri from a file
        Uri outputFileUri = Uri.fromFile(file);
        // Standard Intent action that can be sent to have the
        // camera application capture an image and return it.
        // You will be redirected to camera at this line
        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Add the captured image in the path
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        // Start result method - Method handles the output
        // of the camera activity
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Log message
        Log.i("AndroidProgrammerGuru", "resultCode: " + resultCode);
        switch (resultCode) {
            // When user doesn't capture image, resultcode returns 0
            case 0:
                Log.i("AndroidProgrammerGuru", "User cancelled");
                break;
            // When user captures image, onPhotoTaken an user-defined method
            // to assign the capture image to ImageView
            case -1:
                onPhotoTaken();
                break;
        }
    }

    protected void onPhotoTaken() {
        // Log message
        Log.i("AndroidProgrammerGuru", "onPhotoTaken");
        // Flag used by Activity life cycle methods
        taken = true;
        // Flag used to check whether image captured or not
        imgCapFlag = true;
        // BitmapFactory- Create an object
        BitmapFactory.Options options = new BitmapFactory.Options();
        // Set image size
        options.inSampleSize = 4;
        // Read bitmap from the path where captured image is stored
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        // Set ImageView with the bitmap read in the prev line
        image.setImageBitmap(bitmap);
        // Make the TextView invisible
        field.setVisibility(View.GONE);
        Toast.makeText(Set_Wall_Admin.this,
                path +"photo taken", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i("AndroidProgrammerGuru", "onRestoreInstanceState()");
        if (savedInstanceState.getBoolean(Set_Wall_Admin.PHOTO_TAKEN)) {
            onPhotoTaken();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(Set_Wall_Admin.PHOTO_TAKEN, taken);
    }
}
