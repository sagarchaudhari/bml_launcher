package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by SIMPRO on 12/10/2014.
 */
public class Settings extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

    }


    public void show_pass(View v){
        Intent i = new Intent(this,Sign_up.class);
        startActivity(i);
        this.finish();
    }
    public void show_wall(View v){
        Intent i = new Intent(this,Set_Wallpaper.class);
        startActivity(i);
        this.finish();
    }


    public void show_hide_again(View v){
        Intent i = new Intent(this, MainAct.class);
        startActivity(i);


        this.finish();
    }


}
