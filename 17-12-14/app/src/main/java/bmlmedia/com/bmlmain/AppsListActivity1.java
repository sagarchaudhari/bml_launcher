package bmlmedia.com.bmlmain;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by SIMPRO on 11/28/2014.
 */
public class AppsListActivity1 extends Activity{
	public PackageManager manager;
	public List<AppDetail> apps;
    public static String apps_list_user="first";
    private SharedPreferences mshared;
    public static final String Pref="My_A";

    public GridView list;
    ArrayList<String> UserItems = new ArrayList<String>();

    //Button hide, clear;
    ArrayAdapter<AppDetail> adapter;


    AppDetail k;

    String k1;



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    // The definition of our task class

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = true;

        switch(item.getItemId())
        {
            case R.id.action_pass:
                //  Toast.makeText(this, "welcome admin", Toast.LENGTH_SHORT).show();
                Intent i1 = new Intent(this, Sign_up.class);
                startActivity(i1);
                break;
            case R.id.action_hide:
                //  Toast.makeText(this, "welcome admin", Toast.LENGTH_SHORT).show();
                Intent i2 = new Intent(this, MainAct.class);
                startActivity(i2);

                break;
            case R.id.action_logout:
            //  Toast.makeText(this, "welcome admin", Toast.LENGTH_SHORT).show();
            Intent i3 = new Intent(this, Welcome.class);
            startActivity(i3);

            break;
            default:
                result = super.onOptionsItemSelected(item);

                break;
        }

        return result;

    }


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps_list1);
		list = (GridView) findViewById(R.id.apps_list1);
       // list.setBackgroundResource(R.drawable.shapes2);

        mshared = getSharedPreferences(Pref,MODE_PRIVATE);
        String savedItems = mshared.getString(apps_list_user, "");
        UserItems.addAll(Arrays.asList(savedItems.split(",")));
        new PostTask().execute();

       addClickListener();
    //     my_list();


    }


    private void addClickListener(){
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> av, View v, int pos,
                                    long id) {
                Intent i = manager.getLaunchIntentForPackage(apps.get(pos).name.toString());
                AppsListActivity1.this.startActivity(i);
            }
        });
    }




    // The definition of our task class
	class PostTask extends AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String s;
            manager = getPackageManager();
            apps = new ArrayList<AppDetail>();

            Intent i = new Intent(Intent.ACTION_MAIN, null);

            i.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> availableActivities = manager
                    .queryIntentActivities(i, 0);
            for (ResolveInfo ri : availableActivities) {
                AppDetail app = new AppDetail();
                s=(String)ri.loadLabel(manager);
                //  Toast.makeText(AppsListActivity.this, s, Toast.LENGTH_LONG)
                //        .show();


                app.label = (String) ri.loadLabel(manager);
                app.name = ri.activityInfo.packageName;
                app.icon = ri.activityInfo.loadIcon(manager);
                // app.check = ri.toString();

                //	app.check = false;
                apps.add(app);

            }



        }

        @Override
		protected String doInBackground(String... params) {

            String s;

            adapter = new ArrayAdapter<AppDetail>(AppsListActivity1.this,R.layout.list_item, apps) {

                class myViewHolder {
                    ImageView app_image;
                    TextView app_lab;
                    TextView app_name;

                    myViewHolder(View v) {
                        app_image = (ImageView) v.findViewById(R.id.item_app_icon);
                        app_lab = (TextView) v.findViewById(R.id.item_app_label);

                    }

                }
               @Override
                public View getView(int position, View convertView,
                                    ViewGroup parent) {
                    View row = convertView;

                    myViewHolder holder = null;
                    if (row == null) {
                        row = getLayoutInflater().inflate(
                                R.layout.list_item, parent, false);
                        holder = new myViewHolder(row);
                        row.setTag(holder);
                    } else {
                        holder = (myViewHolder) row.getTag();

                    }

                    k = apps.get(position);


                    holder.app_image.setImageDrawable(k.icon);

                    holder.app_lab.setText(k.label);
                 return row;
                }

            };


            return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
            list.setAdapter(adapter);

		}

	}


}
