package bmlmedia.com.bmlmain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

/**
 * Created by SIMPRO on 12/16/2014.
 */
public class Show_Settings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_setting);
    }

    public void show_wall(View v){
        Intent i = new Intent(this, Set_Wallpaper.class);
        startActivity(i);

        finish();
    }

    public void show_wall_admin(View v){
        Intent i = new Intent(this, Set_Wall_Admin.class);
        startActivity(i);

        finish();
    }

    public void show_log(View v){
        Intent i = new Intent(this, Welcome.class);
        startActivity(i);

        finish();
    }
}
